"""
Naive Bayesian Model Class
"""

import math
import numpy as np


# pylint: disable=E1101
# pylint: disable=C0103
class NaiveBayes(object):
    """The Naive Bayesian class that receives information and handles it """
    def __init__(self, data):
        self._num_classes = self._count_classes(data)
        self._variable_probability_ranges = []
        self._set_variable_probability_ranges(data)
        self._calculate_class_probabilities(data)
        self._calculate_variable_probabilities(data)

    def classify(self, data_point):
        """Classifies a data point based on probabilities worked out."""
        total = 0
        range_classes = self._find_range_classes(data_point)
        probabilities = []
        normalized_probabilities = []
        for class_index in range(self._num_classes):
            class_probability = self._class_probabilites[class_index]
            variable_probability = 1.0
            for variable_index in range(len(range_classes)):
                variable_probability *= \
                    (self._variable_probabilites[class_index]
                     [variable_index][range_classes[variable_index]])
            probabilities.append(class_probability * variable_probability)
        for probability in probabilities:
            total += probability
        for probability in probabilities:
            normalized_probabilities.append(float(probability) / float(total))
        return int(np.argmax(normalized_probabilities) + 1)

    def _find_range_classes(self, data_point):
        """Finds the range classes of the variables of the given data point."""
        range_classes = []
        for index in range(len(data_point))[1:]:
            ranges = self._get_ranges(index - 1)
            item = data_point[index]
            if item <= ranges[0]:
                range_classes.append(0)
            elif (item > ranges[0]) and (item <= ranges[1]):
                range_classes.append(1)
            elif (item > ranges[1]) and (item <= ranges[2]):
                range_classes.append(2)
            elif item > ranges[2]:
                range_classes.append(3)
        return range_classes

    def _calculate_variable_probabilities(self, data):
        """Calculates the variable probabilites."""
        self._variable_probabilites = []
        for class_index in range(self._num_classes):
            self._variable_probabilites.append([])
            for index in range(len(data[0]))[1:]:
                self._variable_probabilites[-1].append([])
                ranges = self._get_ranges(index - 1)
                [counts, total] = \
                    self._count_ranges(data, ranges, class_index + 1, index)
                probabilites = self._calculate_probabilites(counts, total)
                for probability in probabilites:
                    self._variable_probabilites[-1][-1].append(probability)

    @classmethod
    def _calculate_probabilites(cls, counts, total):
        """Calculates probabilites with given counts and totals."""
        probabilites = []
        for count in counts:
            probabilites.append(float(count) / float(total))
        return probabilites

    @classmethod
    def _count_ranges(cls, data, ranges, class_index, index):
        """Counts the amount of value within the given ranges."""
        counts = [0, 0, 0, 0]
        total = 0
        for row in data:
            if row[0] == class_index:
                total += 1
                item = row[index]
                if item <= ranges[0]:
                    counts[0] += 1
                elif (item > ranges[0]) and (item <= ranges[1]):
                    counts[1] += 1
                elif (item > ranges[1]) and (item <= ranges[2]):
                    counts[2] += 1
                elif item > ranges[2]:
                    counts[3] += 1
        for index in range(4):
            if counts[index] == 0:
                counts[index] = 1
        return [counts, total]

    def _get_ranges(self, index):
        """Gets ranges from a certain index."""
        return [
            self._variable_probability_ranges[index][0],
            self._variable_probability_ranges[index][1],
            self._variable_probability_ranges[index][2]
        ]

    def _calculate_class_probabilities(self, data):
        """Calculate class probabilites."""
        self._class_probabilites = []
        for class_index in range(self._num_classes):
            count = 0
            for item in data:
                if item[0] == (class_index + 1):
                    count += 1
            probability = float(count) / float(len(data))
            self._class_probabilites.append(probability)

    def _set_variable_probability_ranges(self, data):
        """Sets the ranges to be used by the various variables."""
        self._variable_probability_ranges = []
        for index in range(len(data[0]))[1:]:
            self._variable_probability_ranges.append([])
            column = [row[index] for row in data]
            mean = self._get_mean(column)
            standard_deviation = self._get_standard_deviation(column, mean)
            self._variable_probability_ranges[-1].append(
                mean - standard_deviation
            )
            self._variable_probability_ranges[-1].append(
                mean
            )
            self._variable_probability_ranges[-1].append(
                mean + standard_deviation
            )

    @classmethod
    def _get_standard_deviation(cls, column, mean):
        """Gets the standard_deviation of a column of data."""
        total = 0
        count = len(column)
        for item in column:
            total += math.pow(item - mean, 2)
        variance = total / count
        return math.sqrt(variance)

    @classmethod
    def _get_mean(cls, column):
        """Gets the mean value of a column of data."""
        total = 0
        count = len(column)
        for item in column:
            total += item
        return total / count

    @classmethod
    def _count_classes(cls, data):
        """Counts the classes based on the data"""
        classes_found = []
        for items in data:
            if items[0] not in classes_found:
                classes_found.append(items[0])
        return len(classes_found)
