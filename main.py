"""
Naive Bayesian Model main file
"""

import naivebayes


def main():
    """The main function which handles running the program."""
    training_data = import_data('train.csv')
    test_data = import_data('test.csv')
    radio_classifier = naivebayes.NaiveBayes(training_data)
    confusions = [[0 for _ in range(15)] for _ in range(15)]
    for test_data_point in test_data:
        classification = radio_classifier.classify(test_data_point)
        print "%d %d" % (radio_classifier.classify(test_data_point), test_data_point[0])
        confusions[classification - 1][int(test_data_point[0] - 1)] += 1
    print confusions


def import_data(filename):
    """Imports data from a file."""
    fileobject = open(filename, 'r')
    line = fileobject.readline()
    inputs = []
    index = 0
    while line != "":
        if ',' in line:
            inputs.append([])
            for item in line.rstrip().split(','):
                inputs[index].append(float(item))
        else:
            inputs.append(float(line))
        line = fileobject.readline()
        index += 1
    return inputs


main()
